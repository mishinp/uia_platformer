﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformerPlayer : MonoBehaviour
{


    public float speed = 250.0f;
    public float jumpForce = 12.0f;

    private Rigidbody2D _body;
    private Animator _anim;

    private BoxCollider2D _box;


    // Start is called before the first frame update
    void Start()
    {
        _body = GetComponent<Rigidbody2D>(); //Риджибади 2д, вместо чарактер контроллерд в 3д
        _anim = GetComponent<Animator>();
        _box = GetComponent<BoxCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        float deltaX = Input.GetAxis("Horizontal") * speed * Time.deltaTime;
        Vector2 movement = new Vector2 (deltaX, _body.velocity.y);
        _body.velocity = movement; //к велосити а не к положению. из-за 2Д
                                   //работа с велосити позволяет учитывать столкновения с коллайдерами другими


        //дальше идет предотвращение возможности прыгать в воздухе
        Vector3 max = _box.bounds.max;
        Vector3 min = _box.bounds.min;

        Vector2 corner1 = new Vector2(max.x, min.y - 0.1f);
        Vector2 corner2 = new Vector2(min.x, min.y - 0.2f);
        Collider2D hit = Physics2D.OverlapArea(corner1, corner2);

        //проверка калайдера
        bool grounded = false;
        if (hit != null)
        {
            grounded = true;
        }


        _body.gravityScale = grounded && deltaX == 0 ? 0 : 1; // отключение имитации силы тяжести при нахождении на повехности
        if (grounded && Input.GetKeyDown(KeyCode.Space))
        {
            _body.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
        }


        //далее идет код приделывающий игрока к движущейся платформе.

        MovingPlatform platform = null;
        if (hit != null)
        {
            platform = hit.GetComponent<MovingPlatform>();
        }

        if(platform != null)
        {
            transform.parent = platform.transform;
        }
        else
        {
            transform.parent = null;
        }
        //приделали к платформе.

        _anim.SetFloat("speed", Mathf.Abs(deltaX)); //скорость больше нуля даже при отрицательных значениях

        //исправим искажание мастаба. возникающее при удочерении платформой
        Vector3 pScale = Vector3.one;
        if (platform != null){
            pScale = platform.transform.localScale;
        }
        if (deltaX != 0)
        {
            transform.localScale = new Vector3(Mathf.Sign(deltaX) / pScale.x, 1 / pScale.y, 1);
        }

        /*
        if (!Mathf.Approximately(deltaX, 0)) //числа типа флоат не всегда совпадают. поэтому approximaletly
        {
            transform.localScale = new Vector3(Mathf.Sign(deltaX), 1, 1); //масштабируем Sign положит или отриц для вопрота влево или вправо
        }
        */
        

    }
}
